#include <stdio.h>
#include <string.h>

struct student{

	char name[20];
	char subject[20];
	float marks;

};

struct student getinfo(){

	struct student s;

	printf("Enter the name of the student:\n");
	scanf("%s" , s.name);

	printf("Enter the subject:\n");
	scanf("%s" , s.subject);

	printf("Enter the marks:\n");
	scanf("%f" , &s.marks);

	if(s.marks<0 || s.marks>100){

		printf("Entered marks are Invalid\n");
		printf("Enter the marks:\t");
		scanf("%f" , &s.marks);
	}

	return s;
}
int main(){

	struct student ar_student[5];
	int i;

	for(i=0 ; i<5 ; i++){
		ar_student[i] = getinfo();

	}
	printf("\n");

	printf("Name \t Subject \t Marks\n");

	for(i=0 ; i<5 ; i++){

		printf("%s \t %s \t %.2f\n" , ar_student[i].name , ar_student[i].subject, ar_student[i].marks);
	}

	return 0;
}
